//OCHOA HUERTA CESIA NUEMI
//4D

// Muestra "Hello world" en la consola
console.log("Hello world");

// Declara variables
var s = "Foo Bar"; // Cadena de texto
let x = 90; // Variable numérica
var y = 89; // Otra variable numérica

// Crea un array con diferentes tipos de datos
var array = [1, 2, 3, 4, 5, "Foo", "Bar", true, false, 2.34, 4.23];

// Crea un objeto con varias propiedades
var obj = {
    first_name: "Foo", // Nombre
    last_name: "Bar", // Apellido
    age: 23, // Edad
    city: "TJ", // Ciudad
    status: true, // Estado
    arr: array // Array dentro del objeto
};

// Usa un bucle 'for' para mostrar números del 0 al 99 en la consola
for (let i = 0; i < 100; i++) {
    console.log(i);
}

// Usa un bucle 'for' para mostrar cada elemento del array en la consola
for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
}

// Intento incorrecto de usar un bucle 'for...of' con la longitud del array
for (let i of array.length) {
    console.log(array[i]);
}

// Usa un bucle 'for...of' para iterar sobre las claves del objeto y mostrar sus valores en la consola
for (let key of Object.keys(obj)) {
    console.log(key + ": " + obj[key]);
}

// Usa un bucle 'for...in' para iterar sobre las propiedades del objeto y mostrar sus valores en la consola
for (let key in obj) {
    console.log(key + ": " + obj[key]);
}

// Usa un bucle 'while' para multiplicar 'i' por 5 hasta que sea mayor o igual a 1000
var i = 1;
while (i < 1000) {
    i *= 5;
    console.log(i);
}

// Usa un bucle 'do...while' para aumentar o disminuir 'i' hasta llegar a 100000000 o 1
var l = true;
do {
    console.log(i);
    if (i == 100000000) {
        l = false;
    } else if (i == 1) {
        l = true;
    }
    if (l) {
        i *= 10;
    } else {
        i /= 10;
    }
} while (true);

// Define una función que espera una cantidad específica de milisegundos
function esperar(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

// Define una función asíncrona que espera 2 segundos antes de imprimir un mensaje en la consola
async function miFuncionConEspera() {
    console.log("Inicio de la función");
    await esperar(2000);
    console.log("Fin de la función después de esperar 2 segundos");
}

// Llama a la función asíncrona
miFuncionConEspera();

// Reasigna la variable 's' a 'true'
s = true;

// Muestra distintas variables y expresiones en la consola
console.log(s);
console.log(x + y);
console.log(s);
console.log(array);
console.log(array[5]);
console.log(obj);
console.log(obj["first_name"]);
console.log(obj.arr);

// Estructura condicional 'if...else'
if (x > y) {
    console.log("si");
} else {
    console.log("no");
}

// Estructura condicional 'switch'
var opc = 1;
switch (opc) {
    case 1:
        console.log("1");
        break;
    case 2:
        console.log("2");
        break;
    case 3:
        console.log("3");
        break;
    default:
        console.log("default");
        break;
}

// Operador ternario para determinar si 'animal' es "Kitty"
var animal = "Kitty";
var hello = (animal === "Kitty") ? "Es un lindo gatito" : "No es un lindo gatito";
console.log(hello);

// Función anidada para imprimir una variable interna
function foo() {
    var a = "gatito";
    function xd() {
        console.log(a);
    }
    xd();
}

foo();

// Define una función 'prism' para calcular el volumen de un prisma
var prism = function(l, w, h) {
    return l * w * h;
};
console.log("Volumen del prisma:" + prism(23, 56, 12));

// Define una función 'prisma' que retorna otra función para calcular el volumen de un prisma
function prisma(l) {
    return function(w) {
        return function(h) {
            return l * w * h;
        };
    };
}
console.log("Volumen del prisma: " + prisma(23)(12)(56));

// Función auto-invocada para imprimir un mensaje
const foo = (function() {
    console.log("Me gustan las patatas");
}());

// Declaración de una función con nombre y una función anónima para sumar dos números
var namedsum = function sum(a, b) {
    return a + b;
};

var anonSum = function(a, b) {
    return a + b;
};

console.log(anonSum(5, 5));

// Función recursiva para imprimir "hello" un número determinado de veces
var say = function say(times) {
    say = undefined;
    if (times > 0) {
        console.log("hello");
        say(times - 1);
    }
};

var saysay = say;
say = "ops";
saysay(5);
console.log(say);

// Función con un parámetro predeterminado para imprimir un mensaje
function foo(msg = "Me gustan las naranjas") {
    console.log(msg);
}
foo();

// Llamada a una API usando fetch para obtener datos de Stack Exchange y JSONPlaceholder
var url = "https://api.stackexchange.com/2.2/questions/featured?order=desc&sort=activity&site=stackoverflow";
var responseData = fetch(url).then(response => response.json());
responseData.then(({ items, has_more, quota_max, quota_remaining }) => {
    for (var { title, score, owner, link, answer_count } of items) {
        console.log('Q:' + title + " owner: " + owner.display_name);
    }
});

var url = "https://jsonplaceholder.typicode.com/users";
var responseData = fetch(url).then(response => response.json())
    .then(response => {
        response.forEach(user => {
            console.log(user.username);
        });
    });

var url = "https://jsonplaceholder.typicode.com/posts";
fetch(url).then(response => response.json())
    .then(response => {
        response.forEach(posts.title);